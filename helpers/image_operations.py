import numpy as np


def standardize(image):
    standardized_image = np.zeros(image.shape)
    # iterate over the `z` dimension
    for z in range(image.shape[2]):
        # get a slice of the image
        # at channel c and z-th dimension `z`
        image_slice = image[:, :, z]

        # subtract the mean from image_slice
        centered = image_slice - np.mean(image_slice)

        # divide by the standard deviation (only if it is different from zero)
        if np.std(centered) != 0:
            centered = centered / np.std(centered)

        # update  the slice of standardized image
        # with the scaled centered and scaled image
        standardized_image[:, :, z] = centered

    return standardized_image
