import csv
import os
from skimage.transform import resize, rescale
import nibabel as nib

import numpy as np
import helpers.image_operations as ioperation


def load_model_images(survival_data, mri_type, training_data_path='data/MICCAI_BraTS2020_TrainingData', count=0, x=120,
                      y=120, z=78, scaled=False):
    cnt = 0
    print(x, y, z)
    input_to_model = np.zeros((count, x, y, z), dtype=np.float16)
    age_list = np.zeros((count, 1))
    with open(survival_data, mode='r') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        # csv_entry_length = len(list(csv_reader)) - 1  # -1 because of header row
        first = True

        for row in csv_reader:
            if first:
                first = False
            else:
                key = row[0]
                age = row[1]
                data = np.zeros((x, y, z))

                folder_path = training_data_path + '/' + key
                modalities = os.listdir(folder_path)
                modalities.sort()
                for j in range(len(modalities)):  # images get loaded here!

                    image_path = folder_path + '/' + modalities[j]
                    if image_path.find(mri_type + '.nii') != -1:
                        img = nib.load(image_path)
                        image_data = img.get_fdata()
                        image_data = np.asarray(image_data)
                        if scaled:
                            image_data = rescale(image_data, 0.5, anti_aliasing=False)
                        # image_data = resize(image_data, (120, 120, 78))
                        image_data = ioperation.standardize(image_data)
                        data = image_data
                        break

                input_to_model[cnt] = data
                age_list[cnt, 0] = age
                cnt += 1

    return input_to_model, age_list, cnt
