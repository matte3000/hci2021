import os

import pandas as pd
import seaborn as sns
import matplotlib as mpl
import matplotlib.pyplot as plt
from tqdm import tqdm
from rich import print

mpl.rcParams['figure.dpi'] = 300

models = ["3dcnn", "3d_scaled", "3d"]
# models = ["3dcnn"]

mris = ["flair", "t1", "t1ce", "t2"]
# mris = ["flair"]

figpath = 'fig'

colors = sns.color_palette()

for model in tqdm(models):
    for mri in mris:
        save_path = figpath + "/" + model
        os.makedirs(save_path, exist_ok=True)

        data_eval = pd.read_json(f"results/surv_pred_{model}_{mri}_stats.json")
        # fig = plt.figure(figsize=(10, 50))
        sns.set(font_scale=1.5)
        sns.jointplot(
            data=data_eval,
            x="ground_truth",
            y="predicted",
            kind="reg",
            xlim=(0, data_eval["ground_truth"].max() + 50),
            ylim=(0, data_eval["predicted"].max() + 50),
        )

        plt.tight_layout()
        plt.savefig(f"{save_path}/{mri}_eval_stats.png")
        plt.close()

        palette = {
            "class 0": "C0",
            "class 1": "C1",
            "class 2": "C2"
        }
        plot = sns.jointplot(
            data=data_eval,
            x="ground_truth",
            y="predicted",
            hue="class_type",
            xlim=(0, data_eval["ground_truth"].max() + 50),
            ylim=(0, data_eval["predicted"].max() + 50),
            palette=palette,
            hue_order=["class 0", "class 1", "class 2"]
        )
        plt.tight_layout()
        plt.savefig(f"{save_path}/{mri}_eval_stats_class.png")
        plt.close()

exit()
