Median:   3955.39185 
Mean:  22853.61970 
Standard SE:  83608.18387
SpearmanR - Correlation:    0.87477 | pValue:    0.00000
Final Accuracy: 0.7370304114490162; MSE: [[22853.598]]
Num Class correct samples: T1 156.0, T2: 79.0, T3: 177.0
Num Class All samples: T1 175, T2: 128, T3: 256
Class accuracies: T1 0.8914285714285715, T2: 0.6171875, T3: 0.69140625
