Median:   1435.68909 
Mean:  13362.35028 
Standard SE:  49180.29202
SpearmanR - Correlation:    0.95133 | pValue:    0.00000
Final Accuracy: 0.8622540250447227; MSE: [[13362.3545]]
Num Class correct samples: T1 177.0, T2: 140.0, T3: 165.0
Num Class All samples: T1 201, T2: 187, T3: 171
Class accuracies: T1 0.8805970149253731, T2: 0.7486631016042781, T3: 0.9649122807017544
