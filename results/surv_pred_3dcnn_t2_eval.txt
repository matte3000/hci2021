Median:   1672.75208 
Mean:  19423.55041 
Standard SE:  74759.26849
SpearmanR - Correlation:    0.94992 | pValue:    0.00000
Final Accuracy: 0.8944543828264758; MSE: [[19423.55]]
Num Class correct samples: T1 177.0, T2: 139.0, T3: 184.0
Num Class All samples: T1 189, T2: 171, T3: 199
Class accuracies: T1 0.9365079365079365, T2: 0.8128654970760234, T3: 0.9246231155778895
